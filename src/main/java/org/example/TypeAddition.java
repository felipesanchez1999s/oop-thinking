package org.example;

public enum TypeAddition {

    SUGGAR("1","SUGGAR"),
    CINNAMON("2","CINNAMON");


    private String name;
    private String key;
    private TypeAddition(String key,String name){
        this.name=name;
        this.key=key;
    }
    public String getName(){
        return this.name;
    }
    public String getKey(){
        return this.key;
    }
}
