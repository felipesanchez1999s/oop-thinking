package org.example;

public abstract class Ingredient {




    public abstract String getName();

    public abstract int getQuantity();

    public abstract String getPackageType();
}
