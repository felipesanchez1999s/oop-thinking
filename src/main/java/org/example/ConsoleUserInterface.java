package org.example;

import java.util.Scanner;

public class ConsoleUserInterface implements UserInterface{
    Scanner scanner=new Scanner(System.in);
    @Override
    public String getResponse(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    @Override
    public void sendMessage(String message) {
        System.out.println(message);
    }
}
