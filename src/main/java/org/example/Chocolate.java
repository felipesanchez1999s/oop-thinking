package org.example;

public class Chocolate extends Ingredient{
    @Override
    public String getName() {
        return "CHOCOLATE";
    }

    @Override
    public int getQuantity() {
        return 2;
    }

    @Override
    public String getPackageType() {
        return "PASTILLAS";
    }
}
