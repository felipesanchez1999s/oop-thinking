package org.example;

public interface Product {
    public Double getPrice();
    public String getName();
}
