package org.example;

public class Water extends Ingredient{
    @Override
    public String getName() {
        return "WATER";
    }

    @Override
    public int getQuantity() {
        return 0;
    }

    @Override
    public String getPackageType() {
        return null;
    }
}
