package org.example;

public class Latte implements Product {
    @Override
    public Double getPrice() {
        return 100d;
    }

    @Override
    public String getName() {
        return "LATTE";
    }
}
