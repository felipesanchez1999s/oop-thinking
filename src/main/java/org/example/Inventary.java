package org.example;

import java.util.ArrayList;
import java.util.List;

public class Inventary {
    private List<Ingredient> ingredients;
    public Inventary(){
        this.ingredients=new ArrayList<>();
    }

    public int amountOf(IngredientType ingredientType){
        int count=0;
        for(Ingredient ingredient:ingredients)
            if(ingredient.getName().equals(ingredientType.getName()))
                count++;
        return count;
    }
    public Ingredient wasteIngredient(IngredientType ingredientType){

        for(Ingredient ingredient:ingredients){
            if(ingredient.getName().equals(ingredientType.getName())){
                Ingredient wasted=ingredient;
                ingredients.remove(wasted);
                return wasted;
            }
        }
        return null;
    }

    public boolean add(String password,Ingredient ingredient){
        if(password.equals("password")){
            this.ingredients.add(ingredient);
            return true;
        }
        return false;
    }
}
