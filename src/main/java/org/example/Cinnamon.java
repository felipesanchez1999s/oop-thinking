package org.example;

public class Cinnamon extends Addition{
    public Cinnamon(Product product) {
        super(product);
    }

    @Override
    public Double getPrice() {
        return 300d+product.getPrice();
    }

    @Override
    public String getName() {
        return product.getName()+" +Cinammon";
    }
}
