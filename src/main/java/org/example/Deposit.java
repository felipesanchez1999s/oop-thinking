package org.example;

public class Deposit {
    private Double balance=0d;
    public void incrementAmount(Double amount){
        balance+=amount;
    }
    public boolean withDraw(Double amount){
        if(balance>=amount){
            balance-=amount;
            return true;
        }
        return false;
    }
}
