package org.example;



public class ProductManager {

    private ProductCooker productCooker;
    public ProductManager(ProductCooker productCooker){
    this.productCooker=productCooker;
    }
    public TypeProduct[] getOffer(){
        return TypeProduct.values();
    }

    public TypeAddition[] getAdditions(){
        return TypeAddition.values();
    }
    public Product dispenseProduct(TypeProduct typeProduct,TypeAddition[]typeAdditions){
    Product product=productCooker.cook(typeProduct,typeAdditions);
    return product;
    }

    public Product getSimulatedProduct(TypeProduct typeProduct,TypeAddition[] additions){
        Product original=null;
        switch (typeProduct){
            case MOCCA:
                original=new Moca();
                break;
            case AMERICANO:
                original=new Americano();
                break;
            case LATTE:
                original=new Latte();
                break;
            default:
                break;
        }
        for(TypeAddition addition:additions){
            switch (addition){
                case SUGGAR:
                    original=new Suggar(original);
                    break;
                case CINNAMON:
                    original=new Cinnamon(original);
                    break;
                default:
                    break;
            }
        }
        return original;
    }
}
