package org.example;

public class Americano implements Product {
    @Override
    public Double getPrice() {
        return 200d;
    }

    @Override
    public String getName() {
        return "AMERICANO";
    }
}
