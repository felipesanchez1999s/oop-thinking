package org.example;

public class Suggar extends Addition {


    public Suggar(Product product){
        super(product);

    }
    @Override
    public Double getPrice() {
        return 200d+product.getPrice();
    }

    @Override
    public String getName() {
        return product.getName()+" + Suggar";
    }


}
