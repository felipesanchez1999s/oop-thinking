package org.example;

public interface UserInterface {

    public String getResponse(String message);
    public void sendMessage(String message);

}
