package org.example;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductCooker {
    private Recipe moccaRecipe;
    private Recipe americanoRecipe;
    private Recipe latteRecipe;

    private Inventary inventary;

    public ProductCooker (Inventary inventary) {
    this.moccaRecipe=new MoccaRecipe();
    this.americanoRecipe=new AmericanoRecipe();
    this.latteRecipe=new LatteRecipe();
    this.inventary=inventary;

    }
    public Product cook(TypeProduct typeProduct,TypeAddition[]typeAdditions){

        Recipe recipe=null;
        switch (typeProduct){
            case MOCCA:
                recipe=moccaRecipe;
                break;
            case AMERICANO:
                recipe=americanoRecipe;
                break;
            case LATTE:
                recipe=latteRecipe;
                break;
            default:
                recipe=null;
                break;
        }

        IngredientType[] required=recipe.requirements();
        IngredientType[] requiredAdittions=mapToIngredients(typeAdditions);
        IngredientType[]finalRequirements=join(required,requiredAdittions);
        Ingredient[]ingredients=getIngredients(finalRequirements);
        if(ingredients==null) {
            System.out.println("XXXXXXXXXX THERE IS NOT ENOUGH INGREDIENTS XXXXX");
        return null;
        }
        return recipe.execute(ingredients);
    }

    private Ingredient []getIngredients(IngredientType[] requirements){
        List<Ingredient>wastedIngredients=new ArrayList<>();
        HashMap<IngredientType,Integer> reqs=new HashMap<>();
        for(IngredientType it:requirements){

            if(reqs.get(it)==null)
                reqs.put(it,1);
            else
                reqs.put(it,reqs.get(it)+1);
        }

        for(IngredientType type:reqs.keySet())
            if(inventary.amountOf(type)<reqs.get(type)){
                System.out.println("NOT ENOUGH "+type.toString()+"\n");
                return null;
            }

        for(IngredientType type:reqs.keySet()){

            for(int i =0;i<reqs.get(type);i++){
                Ingredient wasted=inventary.wasteIngredient(type);
                if(wasted==null) {
                    System.out.println("NOT ENOUGH "+type.toString()+"\n");
                    return null;
                }
                wastedIngredients.add(wasted);
            }
        }

        Ingredient[] result=new Ingredient[wastedIngredients.size()];
        wastedIngredients.toArray(result);
        return result;
    }

    private IngredientType[]mapToIngredients(TypeAddition[]additionsRequirements){
        List<IngredientType> result=new ArrayList<>();
        for(TypeAddition addition:additionsRequirements){
        IngredientType type=IngredientType.valueOf(addition.getName());
        result.add(type);

        }
        IngredientType[] returnValue=new IngredientType[result.size()];
        result.toArray(returnValue);
        return returnValue;
    }
    private IngredientType[] join(IngredientType[] originalIngredients,IngredientType[] additions){
        IngredientType[] result=new IngredientType[originalIngredients.length+additions.length];
        int contadorGeneral=0;
        int contadorLocal=0;
        while(contadorLocal<originalIngredients.length){
            result[contadorGeneral]=originalIngredients[contadorLocal];
            contadorGeneral++;contadorLocal++;
        }
        contadorLocal=0;
        while(contadorLocal<additions.length){
            result[contadorGeneral]=additions[contadorLocal];
            contadorGeneral++;contadorLocal++;
        }
        return result;
    }



}
