package org.example;

public class Milk extends Ingredient {
    @Override
    public String getName() {
        return "MILK";
    }

    @Override
    public int getQuantity() {
        return 15;
    }

    @Override
    public String getPackageType() {
        return "ONZAS";
    }
}
