package org.example;

public class LatteRecipe implements Recipe {


    @Override
    public Product execute(Ingredient[] ingredients) {
        System.out.println("---------COOKING LATTE-------");
        Product latte=new Latte();
        for(Ingredient ingredient:ingredients){
            try {
                TypeAddition addition=TypeAddition.valueOf(ingredient.getName());
                switch (addition){
                    case SUGGAR:
                        latte=new Suggar(latte);
                        break;
                    case CINNAMON:
                        latte=new Cinnamon(latte);
                        break;
                    default:
                        break;
                }

            }catch (Exception e )       {

            }


        }
        return  latte;
    }

    @Override
    public IngredientType[] requirements() {

        return new IngredientType[]{IngredientType.MILK,IngredientType.MILK,IngredientType.ESPRESSO};
    }
}
