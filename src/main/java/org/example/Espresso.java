package org.example;

public class Espresso extends Ingredient{
    @Override
    public String getName() {
        return "ESPRESSO";
    }

    @Override
    public int getQuantity() {
        return 10;
    }

    @Override
    public String getPackageType() {
        return "ONZAS";
    }
}
