package org.example;

import java.util.Arrays;

public class MoccaRecipe implements Recipe {


    @Override
    public Product execute(Ingredient[] ingredients) {
        System.out.println("---------COOKING MOCCA-------");
        Product mocca=new Moca();

        for(Ingredient ingredient:ingredients){
            try {
                TypeAddition addition=TypeAddition.valueOf(ingredient.getName());
                switch (addition){
                    case SUGGAR:
                        mocca=new Suggar(mocca);
                        break;
                    case CINNAMON:
                        mocca=new Cinnamon(mocca);
                        break;
                    default:
                        break;
                }

            }catch (Exception e )       {

            }


        }
        return mocca;
    }

    @Override
    public IngredientType[] requirements() {
        return new IngredientType[]{IngredientType.MILK,IngredientType.ESPRESSO,IngredientType.CHOCOLATE};
    }
}
