package org.example;

public class CinnamonIngredient extends Ingredient{
    @Override
    public String getName() {
        return "CINNAMON";
    }

    @Override
    public int getQuantity() {
        return 10;
    }

    @Override
    public String getPackageType() {
        return "GRAMOS";
    }
}
