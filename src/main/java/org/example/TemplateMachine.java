package org.example;

public abstract class TemplateMachine {
    protected UserInterface userInterface;

    public TemplateMachine(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    public void execute() {


        do {

            String user = userInterface.getResponse("seleccione su tipo de usuario: \n 1. universitario \n 2.Administrador\n 3. salir\n");
            if (user.trim().equals("1")) {
                offerProduct();

                TypeProduct product = choseProduct();
                offerAdittions();
                TypeAddition[] additions=choseAdditions();
                collectMoney();
                if(!applyPayment(product,additions)) {
                    String desicion=userInterface.getResponse("No se suministro suficiente dinero. Selecciona una opcion:\n 1. Continuar\n 2. Salir");
                    if(desicion.trim().equals("1"))
                        continue;
                    else
                        break;
                }
                supplyProduct(product,additions);
                giveCashBack();
            } else if(user.trim().equals("2")) {
                String option =userInterface.getResponse("Seleccione una opcion\n 1.Agregar Ingredientes \n2. Retirar dinero");
                if(option.trim().equals("1")){
                    addIngredient();
                } else if (option.trim().equals("2")) {
                    withdrawMoney();
                }


            } else if (user.trim().equals("3")) {
                break;
            }


        } while (true);

    }

    public abstract void offerProduct();
    public abstract void offerAdittions();

    public abstract void collectMoney();

    public abstract TypeProduct choseProduct();

    public abstract boolean applyPayment(TypeProduct product,TypeAddition[]additions);

    public abstract void supplyProduct(TypeProduct product,TypeAddition[]additions);

    public abstract void giveCashBack();

    public abstract void addIngredient();

    public abstract void withdrawMoney();

    public abstract  TypeAddition[] choseAdditions();
}
