package org.example;

public enum IngredientType {
    MILK("1","MILK"),
    ESPRESSO("2","ESPRESSO"),
    WATER("3","WATER"),
    CHOCOLATE("4","CHOCOLATE"),
    SUGGAR("5","SUGGAR"),
    CINNAMON("6","CINNAMON");
    private String key;
    private String name;

    private IngredientType(String key,String name){
        this.name=name;
        this.key=key;
    }

    public String getName() {
        return name;
    }
    public String getKey(){
        return key;
    }

}
