package org.example;



/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        UserInterface consoleInterface=new ConsoleUserInterface();
        Inventary inventary=new Inventary();
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Milk());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Espresso());
        inventary.add("password",new Water());
        inventary.add("password",new Water());
        inventary.add("password",new Water());
        inventary.add("password",new Water());
        inventary.add("password",new Chocolate());
        inventary.add("password",new Chocolate());
        inventary.add("password",new Chocolate());
        inventary.add("password",new Chocolate());
        inventary.add("password",new SuggarIngredient());
        inventary.add("password",new SuggarIngredient());
        inventary.add("password",new SuggarIngredient());
        inventary.add("password",new SuggarIngredient());
        inventary.add("password",new CinnamonIngredient());
        inventary.add("password",new CinnamonIngredient());
        inventary.add("password",new CinnamonIngredient());
        ProductCooker cooker=new ProductCooker(inventary);
        ProductManager productManager=new ProductManager(cooker);
        Deposit deposit=new Deposit();
        MoneyManager moneyManager=new MoneyManager(deposit);
        CoffeMachine coffeMachine=new CoffeMachine(consoleInterface,productManager,inventary,moneyManager);
        coffeMachine.execute();

    }
}
