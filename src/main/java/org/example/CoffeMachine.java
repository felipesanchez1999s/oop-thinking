package org.example;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoffeMachine extends TemplateMachine {
    private ProductManager productManager;
    private Inventary inventary;
    private MoneyManager moneyManager;


    public CoffeMachine(UserInterface userInterface, ProductManager productManager,
                        Inventary inventary, MoneyManager moneyManager) {
        super(userInterface);
        this.productManager = productManager;
        this.inventary = inventary;
        this.moneyManager = moneyManager;


    }

    @Override
    public void offerProduct() {
        TypeProduct[] products=productManager.getOffer();
        StringBuilder sb=new StringBuilder("Available Products:\n");
        for(TypeProduct product:products){
            sb.append(product.getKey()+". "+product.getName()+"\n");
        }
        userInterface.sendMessage(sb.toString());
    }

    @Override
    public void offerAdittions() {
        TypeAddition[] additions=productManager.getAdditions();
        StringBuilder sb=new StringBuilder("Available Additions:\n");
        for(TypeAddition addition:additions){
            sb.append(addition.getKey()+". "+addition.getName()+"\n");
        }
        userInterface.sendMessage(sb.toString());
    }

    @Override
    public void collectMoney() {
       String moneyCollected= userInterface.getResponse("Inserte el valor del dinero\n");
        Double money=Double.parseDouble(moneyCollected.trim());
        moneyManager.receiveMoney(money);
    }

    @Override
    public TypeProduct choseProduct() {
        String selectedKey=userInterface.getResponse("Por favor seleccione el numero de producto que desea\n");
        TypeProduct[] products=productManager.getOffer();
        for(TypeProduct product:products){
            if(product.getKey().equals(selectedKey.trim())){
                return product;
            }
        }

    return choseProduct();

    }

    @Override
    public boolean applyPayment(TypeProduct product, TypeAddition[] additions) {
        Product simulatedProduct=productManager.getSimulatedProduct(product,additions);
        return moneyManager.applyPay(simulatedProduct);
    }

    @Override
    public void supplyProduct(TypeProduct product, TypeAddition[] additions) {
        userInterface.sendMessage("COOKING PRODUCT...");
        Product dispensedProduct=productManager.dispenseProduct(product,additions);
        if(dispensedProduct!=null)
        userInterface.sendMessage("DISPENSING PRODUCT...:  PRODUCT: "+dispensedProduct.getName()+" PRICE: "+dispensedProduct.getPrice());
        else{
            userInterface.sendMessage("ERROR DISPENSING PRODUCT");
            moneyManager.receiveMoney(productManager.getSimulatedProduct(product,additions).getPrice());
        }
    }


    @Override
    public void giveCashBack() {
        userInterface.sendMessage("GIVING CASHBACK ...");
        double cashback=moneyManager.getCashback();
        userInterface.sendMessage("YOUR CASHBACK IS "+cashback);

    }

    @Override
    public void addIngredient() {

        IngredientType[] types=IngredientType.values();
        StringBuilder sb=new StringBuilder("Ingredientes permitidos:\n");
        for(IngredientType type:types){
            sb.append(type.getKey()+". "+type.getName()+"\n");
        }
        userInterface.sendMessage(sb.toString());
        String selectedKey=userInterface.getResponse("Por favor seleccione el numero del ingrediente que desea insertar\n");

        String pass=userInterface.getResponse("Por favor digite la contraseña\n");
        boolean result=false;
        for(IngredientType ingredientType:types){
            if(ingredientType.getKey().equals(selectedKey.trim())){
                System.out.println("Adding "+ingredientType.toString()+"...");
                switch (ingredientType){
                    case MILK:
                        result=inventary.add(pass,new Milk());
                        break;
                    case CINNAMON:
                        result=inventary.add(pass,new CinnamonIngredient());
                        break;
                    case ESPRESSO:
                        result=inventary.add(pass,new Espresso());
                        break;
                    case WATER:
                        result=inventary.add(pass,new Water());
                        break;
                    case SUGGAR:
                        result=inventary.add(pass,new SuggarIngredient());
                        break;
                    case CHOCOLATE:
                        result=inventary.add(pass,new Chocolate());
                        break;
                    default:
                        break;

                }
            }
        }
        if(result)
            System.out.println("Ingredient added successfully");
    }

    @Override
    public void withdrawMoney() {
    String amount =userInterface.getResponse("ingrese el monto de dinero que desea retirar");
    String pass=userInterface.getResponse("Ingrese la contraseña");
    Double withdrawed=this.moneyManager.withdrawMoney(pass,Double.parseDouble(amount));
        userInterface.sendMessage("monto retirado:"+withdrawed);
    }

    @Override
    public TypeAddition[] choseAdditions() {

        List<TypeAddition> additionList=new ArrayList<>();
        String selectedKey=null;
       do{
            selectedKey=userInterface.getResponse("Por favor seleccione el numero de adicion que desea y si no desea agregar mas " +
                    "adiciones por favor presione 0\n");
        TypeAddition[] adittions=productManager.getAdditions();
        for(TypeAddition addition:adittions){
            if(addition.getKey().equals(selectedKey.trim())){
                additionList.add(addition);
            }
        }
        } while(!selectedKey.equals("0"));
        TypeAddition[] result=new TypeAddition[additionList.size()];
        additionList.toArray(result);
        return result;
    }
}
