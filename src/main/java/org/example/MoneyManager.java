package org.example;

public class MoneyManager {
    private Double partialAmount=0d;
    private Deposit deposit;
    public MoneyManager(Deposit deposit){
        this.deposit=deposit;
    }
    public void receiveMoney(Double money){

    partialAmount+=money;
    }

    public boolean applyPay(Product product){
        if(product.getPrice()<=partialAmount){
            partialAmount-= product.getPrice();
            deposit.incrementAmount(product.getPrice());
            return true;
        }
        return false;
    }

    public Double getCashback(){
        double cashback=this.partialAmount;
        this.partialAmount=0d;
        return cashback;
    }

    public Double withdrawMoney(String password,Double amount){
        if(password.equals("password")){
            if(this.deposit.withDraw(amount))
                return amount;
            System.out.println("not available amount to withdraw");
            return 0d;
        }
        System.out.printf("not authorized to withdraw\n");
        return 0d;
    }
}

