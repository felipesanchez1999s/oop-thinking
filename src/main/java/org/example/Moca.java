package org.example;

public class Moca implements Product{
    @Override
    public Double getPrice() {
        return 300d;
    }

    @Override
    public String getName() {
        return "MOCA";
    }
}
