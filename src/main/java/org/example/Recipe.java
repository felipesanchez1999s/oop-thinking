package org.example;

public interface Recipe {
   public Product execute(Ingredient[] ingredients);
   public IngredientType[] requirements();
}
