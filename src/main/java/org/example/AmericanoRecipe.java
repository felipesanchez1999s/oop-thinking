package org.example;



public class AmericanoRecipe implements Recipe {


    @Override
    public Product execute(Ingredient[] ingredients) {
        System.out.println("---------COOKING AMERICANO-------");
        Product americano=new Americano();
        for(Ingredient ingredient:ingredients){
            try {
                TypeAddition addition=TypeAddition.valueOf(ingredient.getName());
                switch (addition){
                    case SUGGAR:
                        americano=new Suggar(americano);
                        break;
                    case CINNAMON:
                        americano=new Cinnamon(americano);
                        break;
                    default:
                        break;
                }

            }catch (Exception e )       {

            }


            }
        return americano;
    }

    @Override
    public IngredientType[] requirements() {
        return new IngredientType[]{IngredientType.WATER,IngredientType.WATER,IngredientType.ESPRESSO};
    }
}
