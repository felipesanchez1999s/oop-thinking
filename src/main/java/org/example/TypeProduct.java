package org.example;

public enum TypeProduct {
    MOCCA("1","MOCCA"),
    AMERICANO("2","AMERICANO"),
    LATTE("3","LATTE");

    private String name;
    private String key;
    private TypeProduct(String key,String name){
        this.name=name;
        this.key=key;
    }
    public String getName(){
        return this.name;
    }
    public String getKey(){
        return this.key;
    }
}
