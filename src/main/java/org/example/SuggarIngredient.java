package org.example;

public class SuggarIngredient extends Ingredient{
    @Override
    public String getName() {
        return "SUGGAR";
    }

    @Override
    public int getQuantity() {
        return 20;
    }

    @Override
    public String getPackageType() {
        return "GRAMOS";
    }
}
